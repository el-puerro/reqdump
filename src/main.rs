use std::{
    io::{prelude::*, BufRead, BufReader},
    net::{TcpListener, TcpStream},
    thread,
};
use clap::Parser;

/// Stupidly simple HTTP server to dump HTTP header/body information for debugging
#[derive(Parser, Debug)]
#[command(author, version, about, long_about)]
struct Args {
    /// the port to listen on
    #[arg(short, long, default_value_t = 1337)]
    port: u32,
}

fn main() {
    let args = Args::parse();

    let addr: String = format!("{}:{}", "127.0.0.1", args.port);

    let listener = TcpListener::bind(addr.as_str()).unwrap();
    println!("Listening on {} now", addr);

    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                thread::spawn(|| {
                    handle_request(stream);
                });
            }
            Err(err) => {
                eprintln!("Error accepting connection: {}", err);
            }
        }
    }
}

fn handle_request(mut stream: TcpStream) {
    let mut buf_reader = BufReader::new(&mut stream);

    // Collect headers until an empty line is encountered
    let http_req_headers: Vec<_> = buf_reader
        .by_ref()
        .lines()
        .take_while(|x| match x {
            Ok(line) => !line.is_empty(),
            Err(_) => true, // Continue if there's an error in reading a line
        })
        .collect();

    // Print headers without "Ok"
    let http_req_headers: Vec<_> = http_req_headers
        .into_iter()
        .filter_map(|line| line.ok())
        .collect();
    println!("Request headers: {:#?}", http_req_headers);

    // Find Content-Length header
    let content_length: Option<usize> = http_req_headers
        .iter()
        .filter_map(|line| get_content_length(line))
        .next();

    // Check if the request has a body
    if let Some(content_length) = content_length {
        // Additional debug print
        println!("Expected content length: {}", content_length);

        // Collect the body
        let mut body = vec![0; content_length];
        if let Err(err) = buf_reader.read_exact(&mut body) {
            eprintln!("Error reading body: {}", err);
        } else {
            let body_str = String::from_utf8_lossy(&body);
            println!("Request body: {:#?}", body_str);
        }
    } else {
        println!("No request body");
    }

    // Send a simple HTTP response with a 200 OK status code
    let response = "HTTP/1.1 200 OK\r\nContent-Length: 0\r\n\r\n";
    if let Err(err) = stream.write_all(response.as_bytes()) {
        eprintln!("Error sending response: {}", err);
    }
}

fn get_content_length(header: &str) -> Option<usize> {
    const CONTENT_LENGTH: &str = "Content-Length:";
    if let Some(pos) = header.find(CONTENT_LENGTH) {
        let value_start = pos + CONTENT_LENGTH.len();
        if let Some(value) = header[value_start..].trim().split_whitespace().next() {
            if let Ok(length) = value.parse::<usize>() {
                return Some(length);
            }
        }
    }
    None
}
